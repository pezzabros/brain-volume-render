var express = require('express');
var bodyParser = require ('body-parser'); //lib to parse the body in the messages
var path = require('path');
var app = express();

// Define the port to run on
app.set('port', 4000);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'public')));

// Listen for requests
var server = app.listen(app.get('port'), function() {
    var port = server.address().port;
    console.log('Server running on port ' + port);
});
