/**
 * Created by dpezzatini on 02/06/2017.
 */


var loadScene = function( container_id, onloaded ) {
    //LiteSCENE CODE *************************
    var player = new LS.Player( {
        container_id : container_id,
        alpha: false, //enables to have alpha in the canvas to blend with background
        stencil: true,
        redraw: true, //force to redraw
        resources: "scene/",
        autoresize: true, //resize the 3D window if the browser window is resized
        loadingbar: true, //shows loading bar progress
        shaders : "scene/data/shaders.xml",
        proxy: "@/proxy.php?url=" //allows to proxy request to avoid cross domain problems, in this case the @ means same domain, so it will be http://hostname/proxy
    });

    var allow_remote_scenes = false; //allow scenes with full urls? this could be not safe...

    //support for external server
    var data = localStorage.getItem("wgl_user_preferences" );
    if(data)
    {
        var config = JSON.parse(data);
        if(config.modules.Drive && config.modules.Drive.fileserver_files_url)
        {
            allow_remote_scenes = true;
            LS.ResourcesManager.setPath( config.modules.Drive.fileserver_files_url );
        }
    }

    //allow to use Canvas2D call in the WebGLCanvas (this is not mandatory, just useful)
    if( window.enableWebGLCanvas )
        enableWebGLCanvas( gl.canvas );

    if( LS.queryString["debug"] )
        player.enableDebug();

    //player.loadConfig("config.json", onloaded);
    player.loadScene("scene/scene.json", onloaded)
}
