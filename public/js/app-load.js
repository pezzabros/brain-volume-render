/**
 * Created by dpezzatini on 22/05/2017.
 */
/* globals AMI*/

// VJS classes we will be using in this lesson
var LoadersVolume = AMI.default.Loaders.Volume;

// element to contain the progress bar
var container = document.getElementById('container');

// instantiate the loader
var loader = new LoadersVolume(container);

var t2 = [
    'ADNI_941_S_1311_MR_Axial_PD-T2_TSE__br_raw_20070302220705515_86_S27407_I42330.dcm',
    'ADNI_941_S_1311_MR_Axial_PD-T2_TSE__br_raw_20070302220706656_87_S27407_I42330.dcm',
    'ADNI_941_S_1311_MR_Axial_PD-T2_TSE__br_raw_20070302220707046_88_S27407_I42330.dcm',
    'ADNI_941_S_1311_MR_Axial_PD-T2_TSE__br_raw_20070302220707421_89_S27407_I42330.dcm',
    'ADNI_941_S_1311_MR_Axial_PD-T2_TSE__br_raw_20070302220707843_90_S27407_I42330.dcm',
    'ADNI_941_S_1311_MR_Axial_PD-T2_TSE__br_raw_20070302220708203_91_S27407_I42330.dcm',
    'ADNI_941_S_1311_MR_Axial_PD-T2_TSE__br_raw_20070302220708578_92_S27407_I42330.dcm',
    'ADNI_941_S_1311_MR_Axial_PD-T2_TSE__br_raw_20070302220708984_93_S27407_I42330.dcm',
    'ADNI_941_S_1311_MR_Axial_PD-T2_TSE__br_raw_20070302220709390_94_S27407_I42330.dcm'
];
var files = t2.map(function(v) {
    //return 'https://cdn.rawgit.com/FNNDSC/data/master/dicom/adi_brain/' + v;
    return 'http://127.0.0.1:3000/files/ADNI/941_S_1311/Axial_PD-T2_TSE/2007-03-02_15_53_47_0/S27407/' + v;
});


// once all files have been loaded (fetch + parse + add to array)
// merge them into series / stacks / frames
loader.load(files)
    .then(function() {
        // merge files into clean series/stack/frame structure
        var series = loader.data[0].mergeSeries(loader.data);
        loader.free();
        loader = null;

        // series/stacks/frames are ready to be used
        window.console.log(series);

        // Display some content on the DOM
        var seriesIndex = 1;
        for(var mySeries of series) {
            var seriesDiv = document.createElement('div');
            seriesDiv.className += 'indent';
            seriesDiv.insertAdjacentHTML('beforeend',
                '<div> SERIES (' + seriesIndex + '/' + series.length + ')</div>');
            seriesDiv.insertAdjacentHTML('beforeend',
                '<div class="series"> numberOfChannels: '
                + mySeries.numberOfChannels+ '</div>');

            container.appendChild(seriesDiv);

            // loop through stacks
            var stackIndex = 1;
            for(var myStack of mySeries.stack) {
                var stackDiv = document.createElement('div');
                stackDiv.className += 'indent';
                stackDiv.insertAdjacentHTML('beforeend',
                    '<div> STACK (' + stackIndex + '/'
                    + mySeries.stack.length + ')</div>');
                stackDiv.insertAdjacentHTML('beforeend',
                    '<div class="stack"> bitsAllocated: '
                    + myStack.bitsAllocated+ '</div>');

                seriesDiv.appendChild(stackDiv);

                // loop through frames
                var frameIndex = 1;
                for(var myFrame of myStack.frame) {
                    var frameDiv = document.createElement('div');
                    frameDiv.className += 'indent';
                    frameDiv.insertAdjacentHTML('beforeend',
                        '<div> FRAME (' + frameIndex + '/'
                        + myStack.frame.length + ')</div>');
                    frameDiv.insertAdjacentHTML('beforeend',
                        '<div class="frame"> instanceNumber: '
                        + myFrame.instanceNumber+ '</div>');

                    stackDiv.appendChild(frameDiv);
                    frameIndex++;
                }

                stackIndex++;
            }

            seriesIndex++;
        }
    })
    .catch(function(error) {
        window.console.log('oops... something went wrong...');
        window.console.log(error);
    });
