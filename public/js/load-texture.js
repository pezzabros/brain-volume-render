// VJS classes we will be using in this lesson
var LoadersVolume = AMI.default.Loaders.Volume;

// element to contain the progress bar
var container = document.getElementById('container');

// instantiate the loader
var loader = new LoadersVolume(container);

var t2 = [
    '36444280', '36444294', '36444308', '36444322', '36444336',
    '36444350', '36444364', '36444378', '36444392', '36444406',
    '36444420', '36444434', '36444448', '36444462', '36444476',
    '36444490', '36444504', '36444518', '36444532', '36746856',
];
var files = t2.map(function(v) {
    return 'https://cdn.rawgit.com/FNNDSC/data/master/dicom/adi_brain/' + v;
});

var file = 'https://cdn.rawgit.com/FNNDSC/data/master/nifti/eun_brain/eun_uchar_8.nii.gz'


// once all files have been loaded (fetch + parse + add to array)
// merge them into series / stacks / frames
loader.load(files)
    .then(function() {
        // merge files into clean series/stack/frame structure
        var series = loader.data[0].mergeSeries(loader.data);
        loader.free();
        loader = null;

        // series/stacks/frames are ready to be used
        window.console.log(series);

        var canvas = document.getElementById("myCanvas");

        var ctx = canvas.getContext("2d");

        // Display some content on the DOM
        var seriesIndex = 1;
        for(var mySeries of series) {
            // loop through stacks
            var stackIndex = 1;
            for(var myStack of mySeries.stack) {

                // loop through frames
                var frameIndex = 1;

                canvas.setAttribute("height", myStack.frame[0].rows);
                canvas.setAttribute("width", myStack.frame[0].columns * myStack.frame.length);

                for(var myFrame of myStack.frame) {
                    var pixelData = myFrame.pixelData;
                    var imageData = ctx.getImageData(myFrame.columns * (frameIndex-1), 0, myFrame.columns, myFrame.rows);

                    if(myFrame.bitsAllocated == 16){
                        for(var i = 0; i < pixelData.length; i++) {
                            imageData.data[4*i] = (pixelData[i]*255)/4095;
                            imageData.data[4*i+1] = (pixelData[i]*255)/4095;
                            imageData.data[4*i+2] = (pixelData[i]*255)/4095;
                            imageData.data[4*i+3] = 255;
                        }
                    }

                    if(myFrame.bitsAllocated == 8){
                        imageData = new ImageData(pixelData, myFrame.columns, myFrame.rows);
                    }

                    ctx.putImageData(imageData,myFrame.columns * (frameIndex-1), 0);
                    frameIndex++;
                }



                /**/

                stackIndex++;
            }

            seriesIndex++;
        }
    })
    .catch(function(error) {
        window.console.log('oops... something went wrong...');
        window.console.log(error);
    });
