# README #

Web application controlling a volume obtained from brain MRI scan.

Volume render made with WebGlStudio.

### Running locally ###

To download dependencies and run on localhost:4000

```
$ npm install
$ npm run start
```